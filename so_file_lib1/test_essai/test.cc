#include "UsbDevice.hh"
#include <stdio.h>
#include <string.h>
#include "libusb-1.0/libusb.h"
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdint.h>

#include <time.h>


using namespace std;

//#define VENDOR_ID   0xecd6
//#define DEVICE_ID  0x1002
#define uInt32 uint32_t



class g_USBDevice {
	public :
                UsbDevice&  dev() { return *gdev; }
                g_USBDevice() { gdev=NULL; }
                ~g_USBDevice() {}//if (gdev) delete gdev;}
                void Open (uint16_t VENDOR_ID,uint16_t DEVICE_ID)
                    {
                        //cout << hex << VENDOR_ID << endl;
                        if (gdev) {
                                //delete gdev;
                        }
                        gdev= new UsbDevice(VENDOR_ID,DEVICE_ID);
                    }
                void Close () {if (gdev) 
                    {
                        delete gdev;
                    }
		}
	private:
		UsbDevice *gdev;
	
} gdev;



void OpenDevice(uint16_t VENDOR_ID, uint16_t DEVICE_ID)
{
  
   gdev.Open(VENDOR_ID,DEVICE_ID);
  
}

void CloseDevice()
{
 	 gdev.Close();
}


uInt32 WriteDevice (uInt32 address,
		    uInt32 length,
		    uInt32 buffer[512])
{
  	int len;
	len = gdev.dev().write(address,length,buffer);
	//cout << len <<endl;
	 return(len/4);
}

uInt32 ReadDevice (uInt32 address,
	    	   uInt32 length,
	   	   uInt32 buffer[512])
{
 	 int len;
	len =  gdev.dev().read(address,length, buffer);
	// cout << len << endl;
		     return(len/4);
}

uInt32 BulkRead (uInt32 pipe,
		uInt32 length,
		uInt32 buffer[512])
{
	int len;
	len = gdev.dev().Read_Bulk(pipe,length,buffer);
	//cout << "bulk len " << len << endl;
	 return(len);
}
 


int main(void)
{
 int i;
 int data_rd,read_done;
 time_t timer;
 uint32_t buffer_w[512] ,cmd_ctpr[2048];
 uint32_t buffer_r[512];
  
 OpenDevice(0x1556,0x0420);

 cout << "Hello world!" << endl;
    
 for (i=0;i<10;i++) 
	{
	cmd_ctpr[0] = 0X91000000;
 
	WriteDevice (0X2080,1,cmd_ctpr);//Send request read
	read_done = 0;
	while (read_done == 0){
		ReadDevice (0X2080,1,buffer_r);//read result
		read_done = buffer_r[0] & 0x10000;
		cout <<"Done ? :" << hex << read_done << endl;
		}
	data_rd = buffer_r[0] & 0xffff;
	cout << "ADC : " << dec << data_rd << endl;
	}

CloseDevice();
return 0;
}

