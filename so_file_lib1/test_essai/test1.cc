#include "UsbDevice.hh"
#include <stdio.h>
#include <string.h>
#include "libusb-1.0/libusb.h"
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdint.h>
#include <time.h>
#include <unistd.h>


using namespace std;

//#define VENDOR_ID   0xecd6
//#define DEVICE_ID  0x1002
#define uInt32 uint32_t



class g_USBDevice {
	public :
                UsbDevice&  dev() { return *gdev; }
                g_USBDevice() { gdev=NULL; }
                ~g_USBDevice() {}//if (gdev) delete gdev;}
                void Open (uInt32 VENDOR_ID,uInt32 DEVICE_ID) 
                    {
                        cout << hex << VENDOR_ID << endl;
                        if (gdev) {
                            //delete gdev;
                        }
                        gdev= new UsbDevice(VENDOR_ID,DEVICE_ID);
                    }
    
                void Close () {if (gdev)
                    {
                        delete gdev;
                    }
		}
	private:
		UsbDevice *gdev;
	
} gdev;



void OpenDevice(uInt32 VENDOR_ID, uInt32 DEVICE_ID)
{
  
   gdev.Open(VENDOR_ID,DEVICE_ID);
  
}

void CloseDevice()
{
 	 gdev.Close();
}


uInt32 WriteDevice (uInt32 address,
		    uInt32 length,
		    uInt32 buffer[512])
{
  	int len;
	len = gdev.dev().write(address,length,buffer);
	//cout << len <<endl;
	 return(len/4);
}

uInt32 ReadDevice (uInt32 address,
	    	   uInt32 length,
	   	   uInt32 buffer[512])
{
 	 int len;
	len =  gdev.dev().read(address,length, buffer);
	// cout << len << endl;
		     return(len/4);
}

uInt32 BulkRead (uInt32 pipe,
		uInt32 length,
		uInt32 buffer[512])
{
	int len;
	len = gdev.dev().Read_Bulk(pipe,length,buffer);
	//cout << "bulk len " << len << endl;
	 return(len);
}
 
uint32_t send_cmd (uInt32 pipe,
		    uInt32 length,
		    unsigned char* buffer)
{
  	int len;
	len = gdev.dev().send(pipe,length,buffer);
	//cout << len <<endl
  	return (len);
}



int main(void)
{
    int i;
    int data_rd,read_done;
    time_t timer;
    uint32_t buffer_w[512] ,cmd_ctpr[2048];
    uint32_t buffer_r[512];
    OpenDevice(0x1556,0x0420);
    
    cout << "Hello world!" << endl;
    
    cmd_ctpr[0] = 0X12345678;
    WriteDevice (0x4004,1,cmd_ctpr);//Send request read
    sleep(5);
    ReadDevice (0X4004,1,buffer_r);//read result
    cout <<"return ? :" << hex << buffer_r[0] << endl;
    cout <<"return ? :" << hex << buffer_r[1] << endl;
    cout <<"return ? :" << hex << buffer_r[2] << endl;
    cout <<"return ? :" << hex << buffer_r[3] << endl;
    cout <<"return ? :" << hex << buffer_r[4] << endl;
    cout <<"return ? :" << hex << buffer_r[5] << endl;
    cout <<"return ? :" << hex << buffer_r[6] << endl;
    cout <<"return ? :" << hex << buffer_r[7] << endl;
    
    

CloseDevice();
return 0;
}

