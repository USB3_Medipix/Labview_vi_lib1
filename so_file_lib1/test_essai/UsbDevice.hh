//#include "libusb.h"
#include <stdint.h>


class UsbDevice {
public:
    UsbDevice( uint16_t vendorId,
              uint16_t deviceId );
    
    ~UsbDevice();
    
    int write( int unsigned address, // only 31 bit [30..0]; bit 31 = 1 write request
              int length,
              int unsigned buffer[512] );
    
    int read( int unsigned address, // only 31 bit [30..0]; bit 31 = o read request
             int length,
             int unsigned buffer[512]  );
    
    int Read_Bulk (int unsigned pipe,
                   int length,
                   int unsigned buffer[512]);
    
    int send( int pipeNumber,
             int bufferLength,
             unsigned char* buffer );
    
    int receive( int pipeNumber,
                int bufferLength,
                unsigned char* buffer );
    
    
private:
    struct usb_device* device_init();
    
    struct libusb_device_handle* myDeviceHandle;
    int vendorId;
    int deviceId;
    
};
