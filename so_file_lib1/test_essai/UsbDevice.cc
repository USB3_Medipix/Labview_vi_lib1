//
//  UsbDevice.cpp
//  UsbDevice
//
//  Created by DOMINIQUE GIGI on 17/05/2017.
//  Copyright © 2017 DOMINIQUE GIGI. All rights reserved.
//

#include "UsbDevice.hh"
#include "libusb-1.0/libusb.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

using namespace std;

UsbDevice::UsbDevice( uint16_t vendorId,
                     uint16_t deviceId ) {
    //vendorId = newVendorId;
    //this->deviceId = deviceId;
    
    //struct libusb_device *usb_dev;
    int open_status, result;
    uint8_t address;

    //initialize a library session
    result = libusb_init(NULL);
    
    if (result) {
        cout << "Error in initializing libusb library ..." << endl;
                 //return -1;
    }
 
    myDeviceHandle = libusb_open_device_with_vid_pid(NULL,vendorId,deviceId);
    if (myDeviceHandle == NULL) {
        cout << "Not able to open the USB device" << endl;
        //exit;
    }
    
    libusb_device *tdev =libusb_get_device(myDeviceHandle);
    
    address = libusb_get_device_address(tdev) ;
    cout << " Device Found @ Address.. : " << address << endl;
    cout << " USB speed : " << hex << libusb_get_device_speed(tdev) << endl;
    //cout << " Product ID 0x" << hex << libusb_device_descriptor->idProduct << endl;
    
    
    
    open_status = libusb_set_configuration(myDeviceHandle,1);
    cout << "configuration_status = " <<  open_status << endl;
    
    open_status = libusb_claim_interface(myDeviceHandle,0);
    cout << "claim_interface_status = " << open_status << endl;
    
   // open_status = (myDeviceHandle,0);
    //cout << "alternative_interface_status= " << open_status << endl;
    
}


UsbDevice::~UsbDevice() {
    int ret_rls = libusb_release_interface(myDeviceHandle,0); // seems that is not need !!
    int ret_r = libusb_reset_device(myDeviceHandle);
    libusb_close(myDeviceHandle);
    cout << "have closed device ret is " << dec << ret_rls << ret_r << endl;
}



int UsbDevice::write (int unsigned address,
                      int  length,
                      int unsigned buffer[512] )
{
    unsigned char tx_buf[2048];
    int i,result,add[4],len[2],data[4];
    
    
    // prepare address
    if (address >= 0x80000000)
    {
        cout << "This address range is not supported! It should be lower than 0x80000000 !" << endl;
    }
    else {
        add[0]    = (address & 0xff000000)>>24;
        tx_buf[3] = (char) (add[0] | 0x80); //write request
        add[1]    = (address & 0x00ff0000)>>16;
        tx_buf[2] = (char) add[1];
        add[2]	  = (address & 0x0000ff00)>>8;
        tx_buf[1] = (char) add[2];
        add[3]    = (address & 0x000000ff);
        tx_buf[0] = (char) add[3];
        
        //cout << hex << address << " = " << add[0] << add[1] << add[2] << add[3] << endl;
        
        // prepare the length
        
        tx_buf[7] = (char) 0;
        tx_buf[6] = (char) 0;
        len[0]    = (length & 0xff00) >>8;
        tx_buf[5] = (char) len[0];
        
        len[1]    = (length & 0x00ff);
        tx_buf[4] = (char) len[1];
        
        
        //prepare data
        for (i=0; i < length ; i++)
        {
            data[0]     	= (buffer[i] & 0xff000000)>>24;//byte3
            tx_buf[11+(i*4)] = (char) data[0];
            data[1]	    	= (buffer[i] & 0x00ff0000)>>16;// byte 2
            tx_buf[10+(i*4)] = (char) data[1];
            data[2]	    	= (buffer[i] & 0x0000ff00)>>8;//byte 1
            tx_buf[9+(i*4)] = (char) data[2];
            data[3]     	= (buffer[i] & 0x000000ff);// byte 0
            tx_buf[8+(i*4)] = (char) data[3];
            //cout << "loop w : " << i << endl;
            //cout << hex << buffer[i] <<" = "<< data[0] <<data[1] <<data[2] << data[3] <<endl;
            
        }
        // transmit the buffer to USB
        //cout << "data : "<< tx_buf[3] << tx_buf[2]<< tx_buf[1]<< tx_buf[0] << endl;
        result = send(1,(length*4)+8,tx_buf);
    }
    return (result);
}


int UsbDevice::read (int unsigned address,
                     int length,
                     int unsigned buffer[512])
{
    unsigned char rx_buf[2048];
    unsigned char tx_buf[8];
    int i,result,add[4],len[2],data[4],temp[4];
    div_t quotient;
    
    // prepare address
    if (address >= 0x80000000)
    {
        cout << "This address range is not supported! It should be lower than 0x80000000 !" << endl;
        
    }
    else {
        add[0]    = (address & 0xff000000)>>24;
        tx_buf[3] = (char) (add[0] | 0x00); //write request
        add[1]    = (address & 0x00ff0000)>>16;
        tx_buf[2] = (char) add[1];
        add[2]	  = (address & 0x0000ff00)>>8;
        tx_buf[1] = (char) add[2];
        add[3]    = (address & 0x000000ff);
        tx_buf[0] = (char) add[3];
        
        //cout << hex << address << " = " << add[0] << add[1] << add[2] << add[3] << endl;
        
        // prepare the length
        tx_buf[7] = (char) 0;
        tx_buf[6] = (char) 0;
        len[0]    = (length & 0xff00) >>8;
        tx_buf[5] = (char) len[0];
        
        len[1]    = (length & 0x00ff);
        tx_buf[4] = (char) len[1];
        
        //transmit the read request through USB
        result = send(1,8,tx_buf);
        
        result = receive (1,length*4,rx_buf);
        
        for (i=0; i < length; i++)
        {
            temp[0]   = (rx_buf[(i*4)+0] & 0xff);
            temp[1]   = (rx_buf[(i*4)+1] & 0xff);
            temp[2]   = (rx_buf[(i*4)+2] & 0xff);
            temp[3]   = (rx_buf[(i*4)+3] & 0xff);
            //cout << "loop r : " << i << endl;
            //cout <<"read" << hex << temp[0] << temp[1] << temp[2] << temp[3] << endl;
            buffer[i] = (temp[3]<<24 ) + ( temp[2]<<16 )  + (temp[1]<<8 )  + temp[0];
        }
    }
    return (result);
    
}


int UsbDevice::Read_Bulk (int unsigned pipe,
                          int length,
                          int unsigned buffer[512])
{
    unsigned char rx_buf[2048];
    int i,temp[4],result;
    
    result = receive (pipe,length*4,rx_buf);
    
    for (i=0; i < length; i++)
    {
        temp[0]   = (rx_buf[(i*4)+0] & 0xff);
        temp[1]   = (rx_buf[(i*4)+1] & 0xff);
        temp[2]   = (rx_buf[(i*4)+2] & 0xff);
        temp[3]   = (rx_buf[(i*4)+3] & 0xff);
        //cout << "loop r : " << i << endl;
        //cout <<"read" << hex << temp[0] << temp[1] << temp[2] << temp[3] << endl;
        buffer[i] = (temp[3]<<24 ) + ( temp[2]<<16 )  + (temp[1]<<8 )  + temp[0];
    }
    return (result);
}




int
UsbDevice::send ( int pipe,
                 int tx_length,
                 unsigned char* tx_buf)
{
    
    int check,len;
    
    pipe = pipe | 0x0;
    
    //cout << "Send pipe : "<< hex << pipe << " Len : "<< tx_length << " data : "<< tx_buf[3] << tx_buf[2]<< tx_buf[1]<< tx_buf[0] << endl;
    
    check = libusb_bulk_transfer(myDeviceHandle,pipe,tx_buf,tx_length,&len,500);
                              
    if (len != tx_length)printf ("Not all bytes were transmitted !!");
    if (check != 0)printf ("Error on transmittion !!");
    return (len);
}

int
UsbDevice::receive (int pipe,
                    int rx_length,
                    unsigned char*  rx_buf)
{
    int len;
    int check;
    
    //cout << "Pipe :" << pipe << endl;
    //cout << "length :" << rx_length << endl;
    //len = usb_bulk_read(myDeviceHandle,pipe,rx_buf,rx_length,500);
    pipe = pipe | 0x80;
    
    //cout << "Receive pipe : "<< hex << pipe << " Len : "<< rx_length << " data : "<< rx_buf[0] << endl;
    
    check = libusb_bulk_transfer(myDeviceHandle,pipe,rx_buf,rx_length,&len,500);
    
    if (len != rx_length) cout << "Not all bytes were received !! :" << len << endl;
    if (check != 0)printf ("Error on reception !!");

    if (len<0)
    {
        printf ("Error on reception !!");
        printf( "%s\n",libusb_error_name(check));
        printf("%d \n",len);
    }
    
    
    return (len);
}
